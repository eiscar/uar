
# Software 

## Installing:

1. Clone the repository into your local folder 
2. Make sure you have the mbed-cli utilities installed 
3. Set the GCC_ARM path to where you have the ARM compiler installed 
4. Compile with:  mbed compile -t GCC_ARM -m NUCLEO_F746ZG 


# Hardware

## Required parts: 

1. Waterproof case: For example a [PELICAN 1170](https://www.amazon.com/gp/product/B0038VETHS/ref=oh_aui_detailpage_o09_s02?ie=UTF8&psc=1)
2. GPS module with PPS output. Adafruit Ultimate GPS 
3. [External GPS Antenna](https://www.amazon.com/gp/product/B013EUOCZ6/ref=oh_aui_detailpage_o09_s00?ie=UTF8&psc=1)  and [uFl to SMA adapter](https://www.amazon.com/gp/product/B00UTT8842/ref=oh_aui_detailpage_o02_s00?ie=UTF8&psc=1) 
4. [12V Battery Pack](https://www.amazon.com/gp/product/B00MHNQIR2/ref=oh_aui_detailpage_o09_s02?ie=UTF8&psc=1)
5. [Status LED](https://www.amazon.com/gp/product/B00FO3B232/ref=oh_aui_detailpage_o07_s00?ie=UTF8&psc=1)
6. [BNC Bulkhead connector](http://www.mouser.com/Search/ProductDetail.aspx?R=112580virtualkey52330000virtualkey523-112580) 
7. Hydrophone
8. Amplifier board: For the Amplifier board I used the designs from Jim Trezzo at OpenRov available [here](https://github.com/jtrezzo/acoustic-location-system)
9. [STNucleo NUCLEO-F746ZG](http://www.mouser.com/ProductDetail/STMicroelectronics/NUCLEO-F746ZG/?qs=mKNKSX85ZJdK2WTD64xf8A%3D%3D) development board  


## Assembly 

Connect the GPS PPS pin to PIN `PA_3`. Also connect ground and power. Wire the output of the amplifier to pin `PC_0`. Supply power to the amplifier board and status led from the 12V battery. 


![alt text](docs/IMG_7698.JPG "Opened Case")
![alt text](docs/IMG_7699.JPG "Closed Case")

