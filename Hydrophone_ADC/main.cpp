
#include "mbed.h"
#include <cmath>
#include "stm32f7xx_hal_rcc.h"
#include "CustomCircularBuffer.h"
//AnalogIn in(PC_0);


#define BUF_LEN 2500
#define GPS_PPS_PIN PA_3
#define RANGE_12BITS 4095 /* Max value for a full range of 12 bits (4096 values) */
#define LOW_THRESHOLD (RANGE_12BITS * 0.46) // =1.6V
#define HIGH_THRESHOLD (RANGE_12BITS * 0.575)  // =1.9V

Serial pc(USBTX,USBRX,230400);
DigitalOut led(LED1);
DigitalOut led2(LED2);

InterruptIn gps_pps(GPS_PPS_PIN);
InterruptIn user_button(USER_BUTTON);


// Buffers to store the ADC values as well as their timestamp 
volatile uint16_t buffer_u16[BUF_LEN];
volatile float buffer_float[BUF_LEN];

volatile CustomCircularBuffer<uint32_t,BUF_LEN> prebuf;
volatile CustomCircularBuffer<float   ,BUF_LEN> cbuf_time;


volatile int count = 0;
Timer tim;
Timer delayFromSec; 

int counter = 0;

volatile bool peak = false;

int adc_freq = 250000;

static TIM_HandleTypeDef s_TimerInstance;
ADC_HandleTypeDef g_AdcHandle;

//void restartAWD() {
//	__HAL_ADC_ENABLE_IT(&g_AdcHandle,ADC_CR1_AWDIE);
//}

 
void ConfigureADC()
{
    GPIO_InitTypeDef gpioInit;
    ADC_AnalogWDGConfTypeDef AnalogWDGConfig;

    __GPIOC_CLK_ENABLE();
    __ADC1_CLK_ENABLE();
 
    gpioInit.Pin = GPIO_PIN_0;
    gpioInit.Mode = GPIO_MODE_ANALOG;
    gpioInit.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOC, &gpioInit);
 
    HAL_NVIC_SetPriority(ADC_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(ADC_IRQn);
 
    ADC_ChannelConfTypeDef adcChannel;
 
    g_AdcHandle.Instance = ADC1;
 
    g_AdcHandle.Init.ClockPrescaler = ADC_CLOCKPRESCALER_PCLK_DIV2;
    g_AdcHandle.Init.Resolution = ADC_RESOLUTION_12B;
    g_AdcHandle.Init.ScanConvMode = DISABLE;
    g_AdcHandle.Init.ContinuousConvMode = DISABLE;//ENABLE;
    g_AdcHandle.Init.DiscontinuousConvMode = DISABLE;
    g_AdcHandle.Init.NbrOfDiscConversion = 0;
    g_AdcHandle.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING;//ADC_EXTERNALTRIGCONVEDGE_NONE;
    g_AdcHandle.Init.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T8_TRGO;
    g_AdcHandle.Init.DataAlign = ADC_DATAALIGN_RIGHT;
    g_AdcHandle.Init.NbrOfConversion = 1;
    g_AdcHandle.Init.DMAContinuousRequests = ENABLE;
    g_AdcHandle.Init.EOCSelection = DISABLE;
 
    HAL_ADC_Init(&g_AdcHandle);
 
    adcChannel.Channel = ADC_CHANNEL_10;
    adcChannel.Rank = 1;
    adcChannel.SamplingTime = ADC_SAMPLETIME_3CYCLES;
    adcChannel.Offset = 0;
 
    if (HAL_ADC_ConfigChannel(&g_AdcHandle, &adcChannel) != HAL_OK)
    {
        asm("bkpt 255");
    }

    AnalogWDGConfig.WatchdogNumber = 0;
    AnalogWDGConfig.WatchdogMode = ADC_ANALOGWATCHDOG_SINGLE_REG;
    AnalogWDGConfig.HighThreshold = HIGH_THRESHOLD ;
    AnalogWDGConfig.LowThreshold = LOW_THRESHOLD;
    AnalogWDGConfig.Channel = ADC_CHANNEL_10;
    AnalogWDGConfig.ITMode = ENABLE;
    HAL_ADC_AnalogWDGConfig(&g_AdcHandle, &AnalogWDGConfig);
   
}

extern "C"
{
	void HAL_ADC_LevelOutOfWindowCallback(ADC_HandleTypeDef* hadc)
	{
		if (__HAL_ADC_GET_FLAG(hadc, ADC_FLAG_AWD) != RESET) {
			delayFromSec.stop();
			peak = true;    
			// Disable the Analog watchdog interrupt
	        __HAL_ADC_DISABLE_IT(hadc, ADC_CR1_AWDIE);
	        __HAL_ADC_CLEAR_FLAG(hadc, ADC_CR1_AWDIE);
		}
	}

    void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* AdcHandle)
    {	
    	float time_val = tim.read();
    	if (peak) {
	    	buffer_float[count] = time_val;
	    	buffer_u16[count]= HAL_ADC_GetValue(AdcHandle);
	    	if(count++>=BUF_LEN-1) {
	    		HAL_ADC_Stop_IT(&g_AdcHandle);
	    		tim.stop();
	    	}
	    }
		else {
			prebuf.push(HAL_ADC_GetValue(AdcHandle));
			cbuf_time.push(time_val);
		}
	    	
    	
    }

    void ADC_IRQHandler()
    {
        HAL_ADC_IRQHandler(&g_AdcHandle);
    }
}


void init_timer() {

	__TIM8_CLK_ENABLE();
	const uint32_t timFreq = 216000000;
	const uint32_t irqPeriod = 1000;

	s_TimerInstance.Instance = TIM8;
    s_TimerInstance.Init.Prescaler = 107;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   ;//13;
    s_TimerInstance.Init.CounterMode = TIM_COUNTERMODE_UP;
    s_TimerInstance.Init.Period = 3;//50;
    s_TimerInstance.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    s_TimerInstance.Init.RepetitionCounter = 0;
    HAL_TIM_Base_Init(&s_TimerInstance);

    TIM_ClockConfigTypeDef clockDef;
  	clockDef.ClockSource    = TIM_CLOCKSOURCE_INTERNAL;
  	clockDef.ClockPolarity  = TIM_CLOCKPOLARITY_BOTHEDGE;
  	clockDef.ClockPrescaler = TIM_CLOCKPRESCALER_DIV1;
  	clockDef.ClockFilter    = 0;
  	HAL_TIM_ConfigClockSource(&s_TimerInstance, &clockDef);
   
  	TIM_MasterConfigTypeDef sMasterConfig;
  	sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  	HAL_TIMEx_MasterConfigSynchronization(&s_TimerInstance, &sMasterConfig);
  	HAL_TIM_Base_Start(&s_TimerInstance);
}

void resetTime() {
    delayFromSec.reset();
    delayFromSec.start();
    tim.reset();
    tim.start();

    led2 = !led2;
}

uint32_t getTimeFreqs(){

	uint32_t multiplier;
	if (HAL_RCC_GetPCLK1Freq() == HAL_RCC_GetSysClockFreq()) {
		multiplier = 1;
	} else {
	    multiplier = 2;
	}
	return  (multiplier * HAL_RCC_GetPCLK1Freq());
}

int main(void)
{
	// Attach interrupts to GPS PPS pin and user button
  	gps_pps.rise(&resetTime);
    user_button.rise(&resetTime);

    ConfigureADC();
    init_timer();
    HAL_ADC_Start_IT(&g_AdcHandle);

    
    pc.printf("Starting ADC timing.... \n\r"); 
    //pc.printf("%u\n", HAL_RCC_GetSysClockFreq ());
    //pc.printf("Timer clock freq: %u\n", getTimeFreqs());
    while(1) {
	    if(count==BUF_LEN){
		    double timedelay  = delayFromSec.read();
		    double time_end   = tim.read();
		    double time_start = timedelay;
		    double time_el = (time_end-time_start)/BUF_LEN;
		    
		    pc.printf("Took %10.9f to read one ADC value with interrupt (averaged over %i samples) \n\r",time_el,BUF_LEN); 
	        pc.printf("NB with %10.9f at delay: %10.9f \n\r",tim.read(), timedelay);
	       

	        counter = 0; 
		    uint32_t tmp; 
		    float tmp_t;
		    while(prebuf.pop(tmp) && cbuf_time.pop(tmp_t) )  {
		    	pc.printf("%i, %u, %10.6f\n\r",counter++,tmp,tmp_t);
			}
			int circbuflen = counter;
		    for (int i=0;i<BUF_LEN;i++) {
		    	pc.printf("%i, %u, %10.6f\n\r",counter++,buffer_u16[i],buffer_float[i]);
		    }
		   	pc.printf("finished, %i, %i, %i \n\r",circbuflen,BUF_LEN,adc_freq);


		    count = 0;
		 	peak = false;

		 	//Enable adc interrupts as well as analog watchdog
		 	HAL_ADC_Start_IT(&g_AdcHandle);
		 	__HAL_ADC_ENABLE_IT(&g_AdcHandle,ADC_CR1_AWDIE);

		} 
		
	}
}
