#!/usr/bin/python
from __future__ import division, print_function
import serial
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import os



def readdata():
    buflen = 0
    adc_freq = 0
    circbuflen = 0
    delay = 0
    record = False
    t = []
    data = []
    index = []
    lines = []
    ser = serial.Serial('/dev/tty.usbmodem1423', baudrate=230400, timeout=1)  # open serial port
    #ser.reset_input_buffer()
    #ser.flushInput()
    while 1:
        line = ser.readline().replace('\r', '')
        if record:
            if 'finished' in line:
                splitline = line.replace(' ', '').split(',')
                circbuflen = int(splitline[-3])
                buflen = int(splitline[-2])
                adc_freq =int(splitline[-1])
                break
            else:
                lines.append(line)

        if "NB" in line:
            print("New buffer: "+line)
            lines = []
            record = True
            delay = float(line.split(':')[-1])
        if "Circular" in line or 'Trigger' in line:
            pass
            #print(line)

    for line in lines:
        try:
            splitline = line.replace(' ', '').split(',')
            index.append(int(splitline[0]))
            data.append(float(splitline[1])/(2**12))
            new_t = float(splitline[2])
            if len(t) > 0:
                if new_t < t[-1]:
                    new_t += 1
            t.append(new_t)
        except:
            print(line)


    ser.close() # close port

    return t, data, delay, adc_freq, circbuflen, buflen


if __name__=="__main__":

    folder = '/Users/Eduardo/Codigo/Acoustics/pool_test_2/dist_31/'
    print('Waiting for data')
    t, data, delay, adc_freq, circ_buf_len, buf_len = readdata()
    t = np.array(t)
    data = np.array(data)
    print('Adc freq: ',adc_freq,' Hz')
    print('Received buffer length:',buf_len)
    print('Received circular buffer length:',circ_buf_len)
    print('Time delay: ',delay,' s')
    numfiles = len(os.listdir(folder))
    fname = folder+'/data_'+ str(numfiles)+'_'+str(delay)
    data *= 3.3
    np.savez(fname, t, data, adc_freq, buf_len, circ_buf_len, delay)

    plt.plot(t,data)
    plt.ylabel('Signal [V]')
    plt.ylim(0,3.3)
    plt.show()