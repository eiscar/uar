from __future__ import division, print_function
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
import math
import cmath
import plotting_tools as pt

def scipy_spectrogram(data, fs):
    print("Processing Spectrogram ")

    rec_signal = data[0,:] - np.average(data[0,:])
    points = 16384
    f, t, Sxx = signal.spectrogram(rec_signal, fs, window='hamming', nperseg=points, noverlap=0.5*points, nfft=4*points,
    detrend=False, return_onesided=True, scaling='density' )

    plt.pcolormesh(t, f, Sxx)
    plt.ylabel('Frequency [Hz]')
    plt.xlabel('Time [sec]')
    plt.show()


def plot_data(data):

    plt.plt(data[3,:], data[0,:], data[3,:], data[1, :], data[3, :], data[2,:])
    plt.show()


def custom_spectogram(data):
    data = data[1,:] - np.average(data[1,:])

    fs = 25000000
    fft_size = 65536
    win_size = 32768
    overlap_fac = 0.8

    hop_size = np.int32(np.floor(win_size * overlap_fac))
    total_segments = np.int32(np.ceil(len(data) / np.float32(hop_size)))
    #Todo: changing the overlap factor doens't seem to preserve energy, need to fix this
    window = np.hanning(win_size) * overlap_fac * 2
    inner_pad = np.zeros((fft_size * 2) - win_size)

    proc = np.concatenate((data, np.zeros(fft_size)))
    result = np.empty((total_segments, fft_size), dtype=np.float32)

    for i in xrange(total_segments):
        current_hop = hop_size * i
        segment = proc[current_hop:current_hop + win_size]
        windowed = segment * window
        padded = np.append(windowed, inner_pad)
        spectrum = np.fft.fft(padded) / fft_size
        autopower = np.abs(spectrum * np.conj(spectrum))
        result[i, :] = autopower[:fft_size]

    result = 20*np.log10(result)          # scale to db
    #result = np.clip(result, -40, 200)   # clip values

    img = plt.imshow(result, origin='lower', cmap='jet', interpolation='nearest', aspect='auto')
    plt.show()


def detect_peaks(x, mph=None, mpd=1, threshold=0, edge='rising',
                 kpsh=False, valley=False, show=False, ax=None):

    """Detect peaks in data based on their amplitude and other features.

    Parameters
    ----------
    x : 1D array_like
        data.
    mph : {None, number}, optional (default = None)
        detect peaks that are greater than minimum peak height.
    mpd : positive integer, optional (default = 1)
        detect peaks that are at least separated by minimum peak distance (in
        number of data).
    threshold : positive number, optional (default = 0)
        detect peaks (valleys) that are greater (smaller) than `threshold`
        in relation to their immediate neighbors.
    edge : {None, 'rising', 'falling', 'both'}, optional (default = 'rising')
        for a flat peak, keep only the rising edge ('rising'), only the
        falling edge ('falling'), both edges ('both'), or don't detect a
        flat peak (None).
    kpsh : bool, optional (default = False)
        keep peaks with same height even if they are closer than `mpd`.
    valley : bool, optional (default = False)
        if True (1), detect valleys (local minima) instead of peaks.
    show : bool, optional (default = False)
        if True (1), plot data in matplotlib figure.
    ax : a matplotlib.axes.Axes instance, optional (default = None).

    Returns
    -------
    ind : 1D array_like
        indeces of the peaks in `x`.

    Notes
    -----
    The detection of valleys instead of peaks is performed internally by simply
    negating the data: `ind_valleys = detect_peaks(-x)`

    The function can handle NaN's

    See this IPython Notebook [1]_.

    References
    ----------
    .. [1] http://nbviewer.ipython.org/github/demotu/BMC/blob/master/notebooks/DetectPeaks.ipynb

    """

    x = np.atleast_1d(x).astype('float64')
    if x.size < 3:
        return np.array([], dtype=int)
    if valley:
        x = -x
    # find indices of all peaks
    dx = x[1:] - x[:-1]
    # handle NaN's
    indnan = np.where(np.isnan(x))[0]
    if indnan.size:
        x[indnan] = np.inf
        dx[np.where(np.isnan(dx))[0]] = np.inf
    ine, ire, ife = np.array([[], [], []], dtype=int)
    if not edge:
        ine = np.where((np.hstack((dx, 0)) < 0) & (np.hstack((0, dx)) > 0))[0]
    else:
        if edge.lower() in ['rising', 'both']:
            ire = np.where((np.hstack((dx, 0)) <= 0) & (np.hstack((0, dx)) > 0))[0]
        if edge.lower() in ['falling', 'both']:
            ife = np.where((np.hstack((dx, 0)) < 0) & (np.hstack((0, dx)) >= 0))[0]
    ind = np.unique(np.hstack((ine, ire, ife)))
    # handle NaN's
    if ind.size and indnan.size:
        # NaN's and values close to NaN's cannot be peaks
        ind = ind[np.in1d(ind, np.unique(np.hstack((indnan, indnan-1, indnan+1))), invert=True)]
    # first and last values of x cannot be peaks
    if ind.size and ind[0] == 0:
        ind = ind[1:]
    if ind.size and ind[-1] == x.size-1:
        ind = ind[:-1]
    # remove peaks < minimum peak height
    if ind.size and mph is not None:
        ind = ind[x[ind] >= mph]
    # remove peaks - neighbors < threshold
    if ind.size and threshold > 0:
        dx = np.min(np.vstack([x[ind]-x[ind-1], x[ind]-x[ind+1]]), axis=0)
        ind = np.delete(ind, np.where(dx < threshold)[0])
    # detect small peaks closer than minimum peak distance
    if ind.size and mpd > 1:
        ind = ind[np.argsort(x[ind])][::-1]  # sort ind by peak height
        idel = np.zeros(ind.size, dtype=bool)
        for i in range(ind.size):
            if not idel[i]:
                # keep peaks with the same height if kpsh is True
                idel = idel | (ind >= ind[i] - mpd) & (ind <= ind[i] + mpd) \
                    & (x[ind[i]] > x[ind] if kpsh else True)
                idel[i] = 0  # Keep current peak
        # remove the small peaks and sort back the indices by their occurrence
        ind = np.sort(ind[~idel])

    if show:
        if indnan.size:
            x[indnan] = np.nan
        if valley:
            x = -x
        if ax is None:
            _, ax = plt.subplots(1, 1, figsize=(8, 4))

        ax.plot(x, 'b', lw=1)
        if ind.size:
            label = 'valley' if valley else 'peak'
            label = label + 's' if ind.size > 1 else label
            ax.plot(ind, x[ind], '+', mfc=None, mec='r', mew=2, ms=8,
                    label='%d %s' % (ind.size, label))
            ax.legend(loc='best', framealpha=.5, numpoints=1)
        ax.set_xlim(-.02*x.size, x.size*1.02-1)
        ymin, ymax = x[np.isfinite(x)].min(), x[np.isfinite(x)].max()
        yrange = ymax - ymin if ymax > ymin else 1
        ax.set_ylim(ymin - 0.1*yrange, ymax + 0.1*yrange)
        ax.set_xlabel('Data #', fontsize=14)
        ax.set_ylabel('Amplitude', fontsize=14)
        mode = 'Valley detection' if valley else 'Peak detection'
        ax.set_title("%s (mph=%s, mpd=%d, threshold=%s, edge='%s')"
                     % (mode, str(mph), mpd, str(threshold), edge))
        # plt.grid()
        plt.show()

    return ind


def process_peaks(peak_ind):

    peak_diff = [0]
    prev_peak = 0
    for peak in peak_ind:
        peak_diff.append(peak-prev_peak)
        prev_peak = peak

    plt.plot(peak_diff)
    plt.show()


def detect_zero_crossings(x, show=False, ax=None):
    zero_crossings = np.where(np.diff(np.sign(x)))[0]

    if show:
        if ax is None:
                _, ax = plt.subplots(1, 1, figsize=(8, 4))

        ax.plot(x, 'b', lw=1)
        if zero_crossings.size:
            label = 'Zeros'
            label = label + 's' if zero_crossings.size > 1 else label
            ax.plot(zero_crossings, x[zero_crossings], '+', mfc=None, mec='r', mew=2, ms=8,
                    label='%d %s' % (zero_crossings.size, label))
            ax.legend(loc='best', framealpha=.5, numpoints=1)
        ax.set_xlim(-.02*x.size, x.size*1.02-1)
        ymin, ymax = x[np.isfinite(x)].min(), x[np.isfinite(x)].max()
        yrange = ymax - ymin if ymax > ymin else 1
        ax.set_ylim(ymin - 0.1*yrange, ymax + 0.1*yrange)
        ax.set_xlabel('Data #', fontsize=14)
        ax.set_ylabel('Amplitude', fontsize=14)
        ax.set_title("Zero Crossings ")
        # plt.grid()
        plt.show()
    return zero_crossings


def sdft(x, f0, fs, window):

    '''

    :param x: Data Sequence to analyze
    :param f0: Frequency of interest in Hz
    :param fs: Sampling frequency in Hz
    :param window: Window Size
    :return: SDFT array
    '''

    frequency_spacing = fs/window

    k = (f0*window)/fs
    n = int(math.ceil(k))
    print(n)
    Xk = []
    Xk.append(np.fft.fft(x[:window])[n])

    for l in range(window,len(x)):
        Xk.append( (Xk[-1]+x[l]-x[l-window]) * cmath.exp((-1j*2*math.pi*n)/window) ) #According to http://www.comm.toronto.edu/~dimitris/ece431/slidingdft.pdf

    Xkmag = np.absolute(Xk)
    plt.plot(Xkmag)
    plt.show()
    return Xkmag


def cross_correlation_detector(meas_signal, signal_timebase,  ref_signal, show=False, save_fig=False):

    ac_data = (meas_signal-np.mean(meas_signal))[:]  # Subtract the dc component of the signal
    signal_correlation = signal.correlate(ref_signal, ac_data)  #np.correlate(subsignal,signal)
    signal_offset = -(np.argmax(signal_correlation)-(len(ac_data)-1)) #Get offset index
    print('Signal offset:', signal_offset)         # Max offset by len(signal)-1
    print('t[SignalOffset]=', signal_timebase[signal_offset], ' s')
    if signal_timebase[signal_offset] > 1:
        correlation_delay = signal_timebase[signal_offset]-1
    else:
        correlation_delay = signal_timebase[signal_offset]

    aligned_data = np.zeros(len(ac_data))
    aligned_data[signal_offset:signal_offset+len(ref_signal)] = ref_signal

    fig = plt.figure(figsize=(12, 10))
    ax1 = fig.add_subplot(2, 2, 1)
    ax1.plot(signal_timebase, ac_data, color='green')
    ax1.set_title('Measured Signal')
    ax1.set_ylabel('Amplitude [V]', size=20)
    ax1.set_xlabel('Time [s]', size=20)

    ax2 = fig.add_subplot(2, 2, 2)
    ax2.plot(ref_signal)
    ax2.set_title('Reference Signal', size=20)
    ax2.set_ylabel('Amplitude [V]', size=20)
    ax2.set_xlabel('Sample Number', size=20)

    ax3 = fig.add_subplot(2, 2, 3)
    ax3.plot(signal_correlation, color='blue', lw=1)
    ax3.set_xlabel("Offset ", size=20)
    ax3.set_ylabel("Correlation", size=20)
    ax3.set_title('Correlation (refsig,sig)', size=20)

    ax4 = fig.add_subplot(2, 2, 4)
    ax4.plot(signal_timebase, ac_data/np.max(ac_data), color='blue')
    ax4.plot(signal_timebase, aligned_data/np.max(aligned_data), color='green')
    ax4.set_title('Aligned signals', size=20)
    ax4.set_xlabel('Time [s]', size=20)
    ax4.set_ylabel('Normalized \n Amplitude', size=20)
    fig.tight_layout()

    #  Save the subplots individually in the current figure folder
    if save_fig:
        for index, axis in enumerate(fig.get_axes()):
            extent = axis.get_tightbbox(fig.canvas.get_renderer()).transformed(fig.dpi_scale_trans.inverted())
            fig.savefig(pt.getabsolutepath('./ax{}_fig.png'.format(index)), bbox_inches=extent)

    if show:
        plt.show()
    else:
        plt.close(fig)

    return correlation_delay



def tests():
    x =  [1, 2, 3, 4, 5]
    z =  [2, 3, 4, 5, 6]
    true_res = np.fft.fft(z)

    nfftres = np.fft.fft(x)
    window = 5
    #k = 4
    k_vec = range(5)
    xk = (nfftres-1+6)  #*cmath.exp((1j*2*math.pi*k)/window)
    exp_vec = map(lambda k: cmath.exp((1j*2*math.pi*k)/window),k_vec)
    res = np.multiply(xk, exp_vec)
    print(nfftres)
    print(xk)
    print(exp_vec)
    print(" ")
    print (true_res)
    print (res)

if __name__=='__main__':
    t = np.arange(0, 1, 0.000004)   # 250kHz timebase
    data = np.zeros(t.shape)

    data[100000:105000] = map(lambda x: 10*math.sin(2*np.pi*20000*x), t[100000:105000])

    sdft(data,20000,250000,100)