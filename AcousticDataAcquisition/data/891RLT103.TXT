   SWEEP MEASUREMENT
------------------------
Date:       2017/06/17
Time:         02:29:52
HIGH       : 766.4nF 
LOW        :-173.6nF 
STARTFREQ  : 1.000kHz
STOPFREQ   : 60.00kHz
LEVEL      : 1  VRMS
STEP       : 1
AUTOSCALE  : ON 
SCALE      : LINEAR   
  1  FREQ: 1.000kHz  Cs :   6.801 nF 
  2  FREQ: 1.197kHz  Cs :   6.799 nF 
  3  FREQ: 1.393kHz  Cs :   6.797 nF 
  4  FREQ: 1.590kHz  Cs :   6.794 nF 
  5  FREQ: 1.787kHz  Cs :   6.794 nF 
  6  FREQ: 1.983kHz  Cs :   6.793 nF 
  7  FREQ: 2.180kHz  Cs :   6.791 nF 
  8  FREQ: 2.377kHz  Cs :   6.791 nF 
  9  FREQ: 2.573kHz  Cs :   6.790 nF 
 10  FREQ: 2.770kHz  Cs :   6.789 nF 
 11  FREQ: 2.967kHz  Cs :   6.789 nF 
 12  FREQ: 3.163kHz  Cs :   6.789 nF 
 13  FREQ: 3.360kHz  Cs :   6.789 nF 
 14  FREQ: 3.557kHz  Cs :   6.789 nF 
 15  FREQ: 3.753kHz  Cs :   6.790 nF 
 16  FREQ: 3.950kHz  Cs :   6.790 nF 
 17  FREQ: 4.147kHz  Cs :   6.790 nF 
 18  FREQ: 4.343kHz  Cs :   6.790 nF 
 19  FREQ: 4.540kHz  Cs :   6.790 nF 
 20  FREQ: 4.737kHz  Cs :   6.791 nF 
 21  FREQ: 4.933kHz  Cs :   6.792 nF 
 22  FREQ: 5.130kHz  Cs :   6.792 nF 
 23  FREQ: 5.327kHz  Cs :   6.792 nF 
 24  FREQ: 5.523kHz  Cs :   6.793 nF 
 25  FREQ: 5.720kHz  Cs :   6.793 nF 
 26  FREQ: 5.917kHz  Cs :   6.795 nF 
 27  FREQ: 6.113kHz  Cs :   6.794 nF 
 28  FREQ: 6.310kHz  Cs :   6.796 nF 
 29  FREQ: 6.507kHz  Cs :   6.797 nF 
 30  FREQ: 6.703kHz  Cs :   6.798 nF 
 31  FREQ: 6.900kHz  Cs :   6.797 nF 
 32  FREQ: 7.097kHz  Cs :   6.799 nF 
 33  FREQ: 7.293kHz  Cs :   6.806 nF 
 34  FREQ: 7.490kHz  Cs :   6.808 nF 
 35  FREQ: 7.687kHz  Cs :   6.811 nF 
 36  FREQ: 7.883kHz  Cs :   6.812 nF 
 37  FREQ: 8.080kHz  Cs :   6.814 nF 
 38  FREQ: 8.277kHz  Cs :   6.815 nF 
 39  FREQ: 8.473kHz  Cs :   6.816 nF 
 40  FREQ: 8.670kHz  Cs :   6.817 nF 
 41  FREQ: 8.867kHz  Cs :   6.819 nF 
 42  FREQ: 9.063kHz  Cs :   6.820 nF 
 43  FREQ: 9.260kHz  Cs :   6.822 nF 
 44  FREQ: 9.457kHz  Cs :   6.823 nF 
 45  FREQ: 9.653kHz  Cs :   6.827 nF 
 46  FREQ: 9.850kHz  Cs :   6.828 nF 
 47  FREQ: 10.05kHz  Cs :   6.830 nF 
 48  FREQ: 10.24kHz  Cs :   6.831 nF 
 49  FREQ: 10.44kHz  Cs :   6.834 nF 
 50  FREQ: 10.64kHz  Cs :   6.836 nF 
 51  FREQ: 10.83kHz  Cs :   6.838 nF 
 52  FREQ: 11.03kHz  Cs :   6.841 nF 
 53  FREQ: 11.23kHz  Cs :   6.840 nF 
 54  FREQ: 11.42kHz  Cs :   6.843 nF 
 55  FREQ: 11.62kHz  Cs :   6.846 nF 
 56  FREQ: 11.82kHz  Cs :   6.849 nF 
 57  FREQ: 12.01kHz  Cs :   6.852 nF 
 58  FREQ: 12.21kHz  Cs :   6.855 nF 
 59  FREQ: 12.41kHz  Cs :   6.858 nF 
 60  FREQ: 12.60kHz  Cs :   6.861 nF 
 61  FREQ: 12.80kHz  Cs :   6.865 nF 
 62  FREQ: 13.00kHz  Cs :   6.867 nF 
 63  FREQ: 13.19kHz  Cs :   6.869 nF 
 64  FREQ: 13.39kHz  Cs :   6.873 nF 
 65  FREQ: 13.59kHz  Cs :   6.870 nF 
 66  FREQ: 13.78kHz  Cs :   6.874 nF 
 67  FREQ: 13.98kHz  Cs :   6.888 nF 
 68  FREQ: 14.18kHz  Cs :   6.892 nF 
 69  FREQ: 14.37kHz  Cs :   6.894 nF 
 70  FREQ: 14.57kHz  Cs :   6.897 nF 
 71  FREQ: 14.77kHz  Cs :   6.900 nF 
 72  FREQ: 14.96kHz  Cs :   6.904 nF 
 73  FREQ: 15.16kHz  Cs :   6.908 nF 
 74  FREQ: 15.36kHz  Cs :   6.912 nF 
 75  FREQ: 15.55kHz  Cs :   6.920 nF 
 76  FREQ: 15.75kHz  Cs :   6.923 nF 
 77  FREQ: 15.95kHz  Cs :   6.928 nF 
 78  FREQ: 16.14kHz  Cs :   6.932 nF 
 79  FREQ: 16.34kHz  Cs :   6.937 nF 
 80  FREQ: 16.54kHz  Cs :   6.940 nF 
 81  FREQ: 16.73kHz  Cs :   6.943 nF 
 82  FREQ: 16.93kHz  Cs :   6.949 nF 
 83  FREQ: 17.13kHz  Cs :   6.954 nF 
 84  FREQ: 17.32kHz  Cs :   6.958 nF 
 85  FREQ: 17.52kHz  Cs :   6.963 nF 
 86  FREQ: 17.72kHz  Cs :   6.968 nF 
 87  FREQ: 17.91kHz  Cs :   6.973 nF 
 88  FREQ: 18.11kHz  Cs :   6.978 nF 
 89  FREQ: 18.31kHz  Cs :   6.977 nF 
 90  FREQ: 18.50kHz  Cs :   6.982 nF 
 91  FREQ: 18.70kHz  Cs :   6.987 nF 
 92  FREQ: 18.90kHz  Cs :   6.993 nF 
 93  FREQ: 19.09kHz  Cs :   6.998 nF 
 94  FREQ: 19.29kHz  Cs :   7.004 nF 
 95  FREQ: 19.49kHz  Cs :   7.013 nF 
 96  FREQ: 19.68kHz  Cs :   7.019 nF 
 97  FREQ: 19.88kHz  Cs :   7.025 nF 
 98  FREQ: 20.08kHz  Cs :   7.032 nF 
 99  FREQ: 20.27kHz  Cs :   7.038 nF 
100  FREQ: 20.47kHz  Cs :   7.045 nF 
101  FREQ: 20.67kHz  Cs :   7.052 nF 
102  FREQ: 20.86kHz  Cs :   7.059 nF 
103  FREQ: 21.06kHz  Cs :   7.066 nF 
104  FREQ: 21.26kHz  Cs :   7.073 nF 
105  FREQ: 21.45kHz  Cs :   7.080 nF 
106  FREQ: 21.65kHz  Cs :   7.088 nF 
107  FREQ: 21.85kHz  Cs :   7.096 nF 
108  FREQ: 22.04kHz  Cs :   7.103 nF 
109  FREQ: 22.24kHz  Cs :   7.110 nF 
110  FREQ: 22.44kHz  Cs :   7.117 nF 
111  FREQ: 22.63kHz  Cs :   7.125 nF 
112  FREQ: 22.83kHz  Cs :   7.130 nF 
113  FREQ: 23.03kHz  Cs :   7.135 nF 
114  FREQ: 23.22kHz  Cs :   7.143 nF 
115  FREQ: 23.42kHz  Cs :   7.153 nF 
116  FREQ: 23.62kHz  Cs :   7.162 nF 
117  FREQ: 23.81kHz  Cs :   7.171 nF 
118  FREQ: 24.01kHz  Cs :   7.181 nF 
119  FREQ: 24.21kHz  Cs :   7.191 nF 
120  FREQ: 24.40kHz  Cs :   7.200 nF 
121  FREQ: 24.60kHz  Cs :   7.210 nF 
122  FREQ: 24.80kHz  Cs :   7.221 nF 
123  FREQ: 24.99kHz  Cs :   7.231 nF 
124  FREQ: 25.19kHz  Cs :   7.229 nF 
125  FREQ: 25.39kHz  Cs :   7.240 nF 
126  FREQ: 25.58kHz  Cs :   7.264 nF 
127  FREQ: 25.78kHz  Cs :   7.263 nF 
128  FREQ: 25.98kHz  Cs :   7.274 nF 
129  FREQ: 26.17kHz  Cs :   7.285 nF 
130  FREQ: 26.37kHz  Cs :   7.311 nF 
131  FREQ: 26.57kHz  Cs :   7.323 nF 
132  FREQ: 26.76kHz  Cs :   7.335 nF 
133  FREQ: 26.96kHz  Cs :   7.347 nF 
134  FREQ: 27.16kHz  Cs :   7.361 nF 
135  FREQ: 27.35kHz  Cs :   7.360 nF 
136  FREQ: 27.55kHz  Cs :   7.374 nF 
137  FREQ: 27.75kHz  Cs :   7.387 nF 
138  FREQ: 27.94kHz  Cs :   7.402 nF 
139  FREQ: 28.14kHz  Cs :   7.418 nF 
140  FREQ: 28.34kHz  Cs :   7.434 nF 
141  FREQ: 28.53kHz  Cs :   7.450 nF 
142  FREQ: 28.73kHz  Cs :   7.467 nF 
143  FREQ: 28.93kHz  Cs :   7.484 nF 
144  FREQ: 29.12kHz  Cs :   7.501 nF 
145  FREQ: 29.32kHz  Cs :   7.519 nF 
146  FREQ: 29.52kHz  Cs :   7.538 nF 
147  FREQ: 29.71kHz  Cs :   7.555 nF 
148  FREQ: 29.91kHz  Cs :   7.575 nF 
149  FREQ: 30.11kHz  Cs :   7.597 nF 
150  FREQ: 30.30kHz  Cs :   7.617 nF 
151  FREQ: 30.50kHz  Cs :   7.639 nF 
152  FREQ: 30.70kHz  Cs :   7.662 nF 
153  FREQ: 30.89kHz  Cs :   7.681 nF 
154  FREQ: 31.09kHz  Cs :   7.698 nF 
155  FREQ: 31.29kHz  Cs :   7.717 nF 
156  FREQ: 31.48kHz  Cs :   7.740 nF 
157  FREQ: 31.68kHz  Cs :   7.768 nF 
158  FREQ: 31.88kHz  Cs :   7.797 nF 
159  FREQ: 32.07kHz  Cs :   7.824 nF 
160  FREQ: 32.27kHz  Cs :   7.853 nF 
161  FREQ: 32.47kHz  Cs :   7.884 nF 
162  FREQ: 32.66kHz  Cs :   7.915 nF 
163  FREQ: 32.86kHz  Cs :   7.948 nF 
164  FREQ: 33.06kHz  Cs :   7.980 nF 
165  FREQ: 33.25kHz  Cs :   8.013 nF 
166  FREQ: 33.45kHz  Cs :   8.049 nF 
167  FREQ: 33.65kHz  Cs :   8.088 nF 
168  FREQ: 33.84kHz  Cs :   8.126 nF 
169  FREQ: 34.04kHz  Cs :   8.168 nF 
170  FREQ: 34.24kHz  Cs :   8.209 nF 
171  FREQ: 34.43kHz  Cs :   8.253 nF 
172  FREQ: 34.63kHz  Cs :   8.300 nF 
173  FREQ: 34.83kHz  Cs :   8.350 nF 
174  FREQ: 35.02kHz  Cs :   8.400 nF 
175  FREQ: 35.22kHz  Cs :   8.457 nF 
176  FREQ: 35.42kHz  Cs :   8.521 nF 
177  FREQ: 35.61kHz  Cs :   8.579 nF 
178  FREQ: 35.81kHz  Cs :   8.642 nF 
179  FREQ: 36.01kHz  Cs :   8.706 nF 
180  FREQ: 36.20kHz  Cs :   8.771 nF 
181  FREQ: 36.40kHz  Cs :   8.841 nF 
182  FREQ: 36.60kHz  Cs :   8.901 nF 
183  FREQ: 36.79kHz  Cs :   8.978 nF 
184  FREQ: 36.99kHz  Cs :   9.067 nF 
185  FREQ: 37.19kHz  Cs :   9.163 nF 
186  FREQ: 37.38kHz  Cs :   9.259 nF 
187  FREQ: 37.58kHz  Cs :   9.367 nF 
188  FREQ: 37.78kHz  Cs :   9.484 nF 
189  FREQ: 37.97kHz  Cs :   9.600 nF 
190  FREQ: 38.17kHz  Cs :   9.730 nF 
191  FREQ: 38.37kHz  Cs :   9.875 nF 
192  FREQ: 38.56kHz  Cs :   10.02 nF 
193  FREQ: 38.76kHz  Cs :   10.19 nF 
194  FREQ: 38.96kHz  Cs :   10.37 nF 
195  FREQ: 39.15kHz  Cs :   10.56 nF 
196  FREQ: 39.35kHz  Cs :   10.77 nF 
197  FREQ: 39.55kHz  Cs :   10.99 nF 
198  FREQ: 39.74kHz  Cs :   11.23 nF 
199  FREQ: 39.94kHz  Cs :   11.51 nF 
200  FREQ: 40.14kHz  Cs :   11.83 nF 
201  FREQ: 40.33kHz  Cs :   12.19 nF 
202  FREQ: 40.53kHz  Cs :   12.62 nF 
203  FREQ: 40.73kHz  Cs :   13.13 nF 
204  FREQ: 40.92kHz  Cs :   13.67 nF 
205  FREQ: 41.12kHz  Cs :   14.36 nF 
206  FREQ: 41.32kHz  Cs :   15.20 nF 
207  FREQ: 41.51kHz  Cs :   16.17 nF 
208  FREQ: 41.71kHz  Cs :   17.45 nF 
209  FREQ: 41.91kHz  Cs :   19.11 nF 
210  FREQ: 42.10kHz  Cs :   21.18 nF 
211  FREQ: 42.30kHz  Cs :   24.08 nF 
212  FREQ: 42.50kHz  Cs :   28.22 nF 
213  FREQ: 42.69kHz  Cs :   34.51 nF 
214  FREQ: 42.89kHz  Cs :   47.30 nF 
215  FREQ: 43.09kHz  Cs :   83.11 nF 
216  FREQ: 43.28kHz  Cs :   477.6 nF 
217  FREQ: 43.48kHz  Cs :  -100.0 nF 
218  FREQ: 43.68kHz  Cs :  -41.71 nF 
219  FREQ: 43.87kHz  Cs :  -25.37 nF 
220  FREQ: 44.07kHz  Cs :  -17.09 nF 
221  FREQ: 44.27kHz  Cs :  -12.29 nF 
222  FREQ: 44.46kHz  Cs :  -9.297 nF 
223  FREQ: 44.66kHz  Cs :  -7.065 nF 
224  FREQ: 44.86kHz  Cs :  -5.425 nF 
225  FREQ: 45.05kHz  Cs :  -4.227 nF 
226  FREQ: 45.25kHz  Cs :  -3.285 nF 
227  FREQ: 45.45kHz  Cs :  -2.636 nF 
228  FREQ: 45.64kHz  Cs :  -2.410 nF 
229  FREQ: 45.84kHz  Cs :  -4.047 nF 
230  FREQ: 46.04kHz  Cs :   3.806 nF 
231  FREQ: 46.23kHz  Cs :   1.599 nF 
232  FREQ: 46.43kHz  Cs :   1.451 nF 
233  FREQ: 46.63kHz  Cs :   1.575 nF 
234  FREQ: 46.82kHz  Cs :   1.748 nF 
235  FREQ: 47.02kHz  Cs :   1.938 nF 
236  FREQ: 47.22kHz  Cs :   2.114 nF 
237  FREQ: 47.41kHz  Cs :   2.264 nF 
238  FREQ: 47.61kHz  Cs :   2.404 nF 
239  FREQ: 47.81kHz  Cs :   2.543 nF 
240  FREQ: 48.00kHz  Cs :   2.677 nF 
241  FREQ: 48.20kHz  Cs :   2.815 nF 
242  FREQ: 48.40kHz  Cs :   2.948 nF 
243  FREQ: 48.59kHz  Cs :   3.067 nF 
244  FREQ: 48.79kHz  Cs :   3.178 nF 
245  FREQ: 48.99kHz  Cs :   3.283 nF 
246  FREQ: 49.18kHz  Cs :   3.384 nF 
247  FREQ: 49.38kHz  Cs :   3.489 nF 
248  FREQ: 49.58kHz  Cs :   3.588 nF 
249  FREQ: 49.77kHz  Cs :   3.677 nF 
250  FREQ: 49.97kHz  Cs :   3.765 nF 
251  FREQ: 50.17kHz  Cs :   3.850 nF 
252  FREQ: 50.36kHz  Cs :   3.919 nF 
253  FREQ: 50.56kHz  Cs :   3.984 nF 
254  FREQ: 50.76kHz  Cs :   4.064 nF 
255  FREQ: 50.95kHz  Cs :   4.115 nF 
256  FREQ: 51.15kHz  Cs :   4.184 nF 
257  FREQ: 51.35kHz  Cs :   4.240 nF 
258  FREQ: 51.54kHz  Cs :   4.298 nF 
259  FREQ: 51.74kHz  Cs :   4.336 nF 
260  FREQ: 51.94kHz  Cs :   4.397 nF 
261  FREQ: 52.13kHz  Cs :   4.443 nF 
262  FREQ: 52.33kHz  Cs :   4.493 nF 
263  FREQ: 52.53kHz  Cs :   4.536 nF 
264  FREQ: 52.72kHz  Cs :   4.576 nF 
265  FREQ: 52.92kHz  Cs :   4.627 nF 
266  FREQ: 53.12kHz  Cs :   4.657 nF 
267  FREQ: 53.31kHz  Cs :   4.684 nF 
268  FREQ: 53.51kHz  Cs :   4.729 nF 
269  FREQ: 53.71kHz  Cs :   4.756 nF 
270  FREQ: 53.90kHz  Cs :   4.790 nF 
271  FREQ: 54.10kHz  Cs :   4.838 nF 
272  FREQ: 54.30kHz  Cs :   4.870 nF 
273  FREQ: 54.49kHz  Cs :   4.888 nF 
274  FREQ: 54.69kHz  Cs :   4.927 nF 
275  FREQ: 54.89kHz  Cs :   4.951 nF 
276  FREQ: 55.08kHz  Cs :   4.990 nF 
277  FREQ: 55.28kHz  Cs :   5.008 nF 
278  FREQ: 55.48kHz  Cs :   5.052 nF 
279  FREQ: 55.67kHz  Cs :   5.066 nF 
280  FREQ: 55.87kHz  Cs :   5.092 nF 
281  FREQ: 56.07kHz  Cs :   5.124 nF 
282  FREQ: 56.26kHz  Cs :   5.157 nF 
283  FREQ: 56.46kHz  Cs :   5.174 nF 
284  FREQ: 56.66kHz  Cs :   5.202 nF 
285  FREQ: 56.85kHz  Cs :   5.225 nF 
286  FREQ: 57.05kHz  Cs :   5.264 nF 
287  FREQ: 57.25kHz  Cs :   5.274 nF 
288  FREQ: 57.44kHz  Cs :   5.311 nF 
289  FREQ: 57.64kHz  Cs :   5.332 nF 
290  FREQ: 57.84kHz  Cs :   5.351 nF 
291  FREQ: 58.03kHz  Cs :   5.389 nF 
292  FREQ: 58.23kHz  Cs :   5.405 nF 
293  FREQ: 58.43kHz  Cs :   5.441 nF 
294  FREQ: 58.62kHz  Cs :   5.463 nF 
295  FREQ: 58.82kHz  Cs :   5.495 nF 
296  FREQ: 59.02kHz  Cs :   5.523 nF 
297  FREQ: 59.21kHz  Cs :   5.527 nF 
298  FREQ: 59.41kHz  Cs :   5.549 nF 
299  FREQ: 59.61kHz  Cs :   5.543 nF 
300  FREQ: 59.80kHz  Cs :   5.536 nF 
301  FREQ: 60.00kHz  Cs :   5.517 nF 
