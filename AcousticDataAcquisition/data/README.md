891100.TXT ----- DL3 Transducer Impedance vs frequency, Bare Ceramic ring 
891101.TXT ----- DL3 Transducer Capacitance vs frequency, Bare Ceramic ring
891102.TXT ----- DL3 Transducer Impedance vs frequency,  Ceramic ring screwed on base
891103.TXT ----- DL3 Transducer Capacitance vs frequency,  Ceramic ring screwed on base
891104.TXT ----- DL3 Transducer Impedance vs frequency,  Complete, potted ceramic ring 
891105.TXT ----- DL3 Transducer Capacitance vs frequency,  Complete, potted ceramic ring

891200.TXT ----- DL4 Transducer Impedance vs frequency, Bare Ceramic ring 
891201.TXT ----- DL4 Transducer Capacitance vs frequency, Bare Ceramic ring
891202.TXT ----- DL4 Transducer Impedance vs frequency,  Ceramic ring screwed on base
891203.TXT ----- DL4 Transducer Capacitance vs frequency,  Ceramic ring screwed on base
891204.TXT ----- DL4 Transducer Impedance vs frequency,  Complete, potted ceramic ring 
891205.TXT ----- DL4 Transducer Capacitance vs frequency,  Complete, potted ceramic ring

891300.TXT ----- DL5 Transducer Impedance vs frequency, Bare Ceramic ring 
891301.TXT ----- DL5 Transducer Capacitance vs frequency, Bare Ceramic ring
891302.TXT ----- DL5 Transducer Impedance vs frequency,  Ceramic ring screwed on base
891303.TXT ----- DL5 Transducer Capacitance vs frequency,  Ceramic ring screwed on base
891304.TXT ----- DL5 Transducer Impedance vs frequency,  Complete, potted ceramic ring 
891305.TXT ----- DL5 Transducer Capacitance vs frequency,  Complete, potted ceramic ring

891400.TXT ----- DL6 Transducer Impedance vs frequency, Bare Ceramic ring 
891401.TXT ----- DL6 Transducer Capacitance vs frequency, Bare Ceramic ring
891402.TXT ----- DL6 Transducer Impedance vs frequency,  Ceramic ring screwed on base
891403.TXT ----- DL6 Transducer Capacitance vs frequency,  Ceramic ring screwed on base
891404.TXT ----- DL6 Transducer Impedance vs frequency,  Complete, potted ceramic ring 
891405.TXT ----- DL6 Transducer Capacitance vs frequency,  Complete, potted ceramic ring


891702.TXT ----- DL7 Transducer Impedance vs frequency,  Ceramic rings screwed on base
891703.TXT ----- DL7 Transducer Capacitance vs frequency,  Ceramic rings screwed on base
891704.TXT ----- DL7 Transducer Impedance vs frequency,  Complete, potted ceramic ring 
891705.TXT ----- DL7 Transducer Capacitance vs frequency,  Complete, potted ceramic ring

DL4-_RVS_TESTS.csv  ----- Voltage response of the DL4 transducer with the RA1 amplifier and the AS1 with the RA2 amplifier as reference
DL5-_RVS_TESTS.csv  ----- Voltage response of the DL5 transducer with the RA1 amplifier and the AS1 with the RA2 amplifier as reference
DL6-_RVS_TESTS.csv  ----- Voltage response of the DL6 transducer with the RA1 amplifier and the AS1 with the RA2 amplifier as reference
DL7-_RVS_TESTS.csv  ----- Voltage response of the DL7 transducer with the RA1 amplifier and the AS1 with the RA2 amplifier as reference, not good
DL7-_RVS_TESTS_2.csv  ----- Voltage response of the DL7 transducer with the RA1 amplifier and the AS1 with the RA2 amplifier as reference, not good
DL7-_RVS_TESTS_2.csv  ----- Voltage response of the DL7 transducer with the RA1 amplifier and the AS1 with the RA2 amplifier as reference

RA1-Experimental_Bode.csv ---- Gain vs Frequency response of the RA1 receiver amplifier obatined experimentaly 
RA2-Experimental_Bode.csv ---- Gain vs Frequency response of the RA2 receiver amplifier obatined experimentaly 


Transducers: 
* DL3 & DL4: Air gapped variant
* DL5 Solid urethane core with cork/neoprene gasket ring
* DL6 Solid core 
* DL7 Double stack wired in series with air gapped cores
			 