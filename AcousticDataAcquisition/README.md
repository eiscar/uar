# Acoustic Data Acquisition and Processing #

## Data folder ## 

1. Amplifier_Transponder_Characteristics.csv ---> Custom Amplifier response using AS-1 as reference hydrophone
2. Receiver.bod --> Bode plot generated with MultiSim for the Receiver amplifier circuit board 
3. Transmitter.bod --> Bode plot generated with MultiSim for the Transmitter amplifier circuit board

## Scripts ## 

1. plotting_tools.py --> Script to plot different transducer properties 
2. capture_data.py   -->  Connect to the STM-32 receiver board through serial and load the acquired data
3. experiments_analyzer.py --> Script to analyze the acoustic data stream 
4. signal_processing.py --> Signal processing functions
5. acoustic_utilities.py --> Set of functions to compute multiple underwater acoustic relations