# UAR : Underwater Acoustic Ranging 

This repository is a collection of software and hardware design files and instructions to get started experimenting with Underwater acoustics to determine the range to a target. For detailed instructions on the different components please refer to the [wiki](https://bitbucket.org/eiscar/uar/wiki/Home). 
If you have any questions about the transducers construction, characterization, electronics or any other part of the project please contact us.

# Folder Content: 

 1. Hydrophone_ADC: Software to run on a STM32 F746ZG development board and sample incoming signal
 2. Hydrophone_Signal_Generator: Software to run on a STM32 F746ZG development board and sample incoming signal
 3. Transducers: 	Hardware design files for the construction of underwater transducers
 4. Amplifiers: 	PCB design files and schematics for transmit and receive amplifiers 

# Current state: 
The project as shown is able to send and detect acoustic signals through water with ranges up to 20m. Time synchronization is obtained through GPS.  Due to the high drift of the oscillator on the microcontroller board, the system is restricted to use on the water surface.  


# To do list: 
* Accurate clock source for maintaining time synchronization underwater over periods of a few hours to a day. 
* Increase transmit amplifier power