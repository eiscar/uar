#include "mbed.h"

//AnalogIn in(PA_3);

#if !DEVICE_ANALOGOUT
#error You cannot use this example as the AnalogOut is not supported on this device.
#else
//AnalogOut out(PA_4);
#endif

#define GPS_PPS_PIN PC_0
#define WAVEFORM_OUT_PIN PA_3

Serial pc(USBTX,USBRX);
DigitalOut myled(LED1);
DigitalOut myled2(LED2);

DigitalOut wav_pin(WAVEFORM_OUT_PIN);
//DigitalIn mybutton(USER_BUTTON);


//Timer t;
Ticker fun_gen_freq;
Timeout fun_gen_off;
InterruptIn button(USER_BUTTON);
InterruptIn gps_pps(GPS_PPS_PIN);

void turn_off() {
    fun_gen_freq.detach();
}

void flip_pin() {
    wav_pin = !wav_pin;
}

void send() {
    fun_gen_freq.attach(&flip_pin,0.000013);
    fun_gen_off.attach(&turn_off,0.0005);
    myled2 = !myled2;

}

int main() {

   //Atach the callback to the serial port receive method
    
    button.rise(&send);
    gps_pps.rise(&send);
    pc.printf("started\n\r");
    while (true) {
        myled = !myled;
        wait_ms(500);
    }
}

